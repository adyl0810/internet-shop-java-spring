'use strict';

$('#loadNext').on('click', function () { //При клике по элементу с id=loadNext выполнять...
    $.ajax({
        url: "http://localhost:8080/api/products", //Путь к файлу, который нужно подгрузить
        type: "GET",
        success: function (response) {
            $('#products-container').append(response); //Подгрузка внутрь блока с id=products-container
        }
    });
});

const productsContainer = $('#products-container');

$("#submit").on('click', function () {
    let search = $("#search").val();
    $.ajax({
        url: "http://localhost:8080/api/products",
        data: {
            name: search
        },
        method: "GET"
    }).done(function (data) {
        productsContainer.empty()
        for (let i = 0; i < data.length; i++) {
            productsContainer.append(`
            <div class="col">
                <div class="card h-100">
                    <img src="images/products/${data[i].image}" class="card-img-top" alt="${data[i].image}">
                        <div class="card-body">
                            <a href="/product/${data[i].id}"><h5 class="card-title">${data[i].name}</h5></a>
                            <p class="card-text">${data[i].description}</p>
                            <a href="/product/${data[i].id}" class="btn btn-primary">Add To Cart</a>
                        </div>
                </div>
            </div>`)
        }
    });
})