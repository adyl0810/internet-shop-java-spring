USE `onlineshop`;

ALTER TABLE `cart_items`
    ADD COLUMN `status` VARCHAR(32) NOT NULL DEFAULT 'PENDING' AFTER `quantity`;