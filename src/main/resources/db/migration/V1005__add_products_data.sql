USE `onlineshop`;

INSERT INTO `products` (`name`, `image`, `quantity`, `description`, `price`, `brand_id`, `category_id`)
VALUES ('Dishwasher', 'dishwasher.jpg', 2, 'Tefal Dishwasher', 100, 1, 1),
       ('Electric Kettle', 'electric-kettle.jpg', 4, 'Tefal Electric Kettle', 40, 1, 2),
       ('Fridge', 'fridge.jpg', 2, 'Tefal Fridge', 200, 1, 3),
       ('Hair Dryer', 'hair-dryer.jpg', 4, 'Tefal Hair Dryer', 20, 1, 4),
       ('Iron', 'iron.jpg', 4, 'Tefal Iron', 30, 1, 5),
       ('Microwave Oven', 'microwave-oven.jpg', 3, 'Tefal Microwave Oven', 75, 1, 6),
       ('Oven', 'oven.jpg', 2, 'Tefal Oven', 125, 1, 7),
       ('Television', 'tv.jpg', 3, 'Tefal Television', 150, 1, 8),
       ('Vacuum Cleaner', 'vacuum-cleaner.jpg', 3, 'Tefal Vacuum Cleaner', 40, 1, 9),
       ('Washing Machine', 'washing-machine.png', 2, 'Tefal Washing Machine', 125, 1, 10),

       ('Dishwasher', 'dishwasher.jpg', 2, 'LG Dishwasher', 100, 2, 1),
       ('Electric Kettle', 'electric-kettle.jpg', 4, 'LG Electric Kettle', 40, 2, 2),
       ('Fridge', 'fridge.jpg', 2, 'LG Fridge', 200, 2, 3),
       ('Hair Dryer', 'hair-dryer.jpg', 4, 'LG Hair Dryer', 20, 2, 4),
       ('Iron', 'iron.jpg', 4, 'LG Iron', 30, 2, 5),
       ('Microwave Oven', 'microwave-oven.jpg', 3, 'LG Microwave Oven', 75, 2, 6),
       ('Oven', 'oven.jpg', 2, 'LG Oven', 125, 2, 7),
       ('Television', 'tv.jpg', 3, 'LG Television', 150, 2, 8),
       ('Vacuum Cleaner', 'vacuum-cleaner.jpg', 3, 'LG Vacuum Cleaner', 40, 2, 9),
       ('Washing Machine', 'washing-machine.png', 2, 'LG Washing Machine', 125, 2, 10),

       ('Dishwasher', 'dishwasher.jpg', 2, 'Samsung Dishwasher', 100, 3, 1),
       ('Electric Kettle', 'electric-kettle.jpg', 4, 'Samsung Electric Kettle', 40, 3, 2),
       ('Fridge', 'fridge.jpg', 2, 'Samsung Fridge', 200, 3, 3),
       ('Hair Dryer', 'hair-dryer.jpg', 4, 'Samsung Hair Dryer', 20, 3, 4),
       ('Iron', 'iron.jpg', 4, 'Samsung Iron', 30, 3, 5),
       ('Microwave Oven', 'microwave-oven.jpg', 3, 'Samsung Microwave Oven', 75, 3, 6),
       ('Oven', 'oven.jpg', 2, 'Samsung Oven', 125, 3, 7),
       ('Television', 'tv.jpg', 3, 'Samsung Television', 150, 3, 8),
       ('Vacuum Cleaner', 'vacuum-cleaner.jpg', 3, 'Samsung Vacuum Cleaner', 40, 3, 9),
       ('Washing Machine', 'washing-machine.png', 2, 'Samsung Washing Machine', 125, 3, 10),

       ('Dishwasher', 'dishwasher.jpg', 2, 'AEG Dishwasher', 100, 4, 1),
       ('Electric Kettle', 'electric-kettle.jpg', 4, 'AEG Electric Kettle', 40, 4, 2),
       ('Fridge', 'fridge.jpg', 2, 'AEG Fridge', 200, 4, 3),
       ('Hair Dryer', 'hair-dryer.jpg', 4, 'AEG Hair Dryer', 20, 4, 4),
       ('Iron', 'iron.jpg', 4, 'AEG Iron', 30, 4, 5),
       ('Microwave Oven', 'microwave-oven.jpg', 3, 'AEG Microwave Oven', 75, 4, 6),
       ('Oven', 'oven.jpg', 2, 'AEG Oven', 125, 4, 7),
       ('Television', 'tv.jpg', 3, 'AEG Television', 150, 4, 8),
       ('Vacuum Cleaner', 'vacuum-cleaner.jpg', 3, 'AEG Vacuum Cleaner', 40, 4, 9),
       ('Washing Machine', 'washing-machine.png', 2, 'AEG Washing Machine', 125, 4, 10),

       ('Dishwasher', 'dishwasher.jpg', 2, 'Bosch Dishwasher', 100, 5, 1),
       ('Electric Kettle', 'electric-kettle.jpg', 4, 'Bosch Electric Kettle', 40, 5, 2),
       ('Fridge', 'fridge.jpg', 2, 'Bosch Fridge', 200, 5, 3),
       ('Hair Dryer', 'hair-dryer.jpg', 4, 'Bosch Hair Dryer', 20, 5, 4),
       ('Iron', 'iron.jpg', 4, 'Bosch Iron', 30, 5, 5),
       ('Microwave Oven', 'microwave-oven.jpg', 3, 'Bosch Microwave Oven', 75, 5, 6),
       ('Oven', 'oven.jpg', 2, 'Bosch Oven', 125, 5, 7),
       ('Television', 'tv.jpg', 3, 'Bosch Television', 150, 5, 8),
       ('Vacuum Cleaner', 'vacuum-cleaner.jpg', 3, 'Bosch Vacuum Cleaner', 40, 5, 9),
       ('Washing Machine', 'washing-machine.png', 2, 'Bosch Washing Machine', 125, 5, 10);