USE `onlineshop`;
INSERT INTO `brands` (`name`, `icon`)
VALUES ('Tefal', 'tefal-logo.png'),
       ('LG','lg-logo.png'),
       ('Samsung','samsung-logo.png'),
       ('AEG','aeg-logo.png'),
       ('Bosch','bosch-logo.png');