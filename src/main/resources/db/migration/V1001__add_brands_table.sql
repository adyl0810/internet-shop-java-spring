USE `onlineshop`;

CREATE TABLE `brands`
(
    `id` INT auto_increment NOT NULL,
    `name` VARCHAR(128) NOT NULL,
    `icon` VARCHAR(128) NOT NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `products`
    ADD COLUMN `brand_id` INT NOT NULL AFTER `price`,
    ADD CONSTRAINT `fk_product_brands`
        FOREIGN KEY (`brand_id`)
            REFERENCES `brands` (`id`);