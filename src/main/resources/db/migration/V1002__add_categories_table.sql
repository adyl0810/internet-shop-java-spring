USE `onlineshop`;

CREATE TABLE `categories`
(
    `id` INT auto_increment NOT NULL,
    `name` VARCHAR(128) NOT NULL,
    `icon` VARCHAR(128) NOT NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `products`
    ADD COLUMN `category_id` INT NOT NULL AFTER `brand_id`,
    ADD CONSTRAINT `fk_product_categories`
        FOREIGN KEY (`category_id`)
            REFERENCES `categories` (`id`);