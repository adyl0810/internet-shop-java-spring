USE `onlineshop`;

CREATE TABLE `products` (
    `id` INT auto_increment NOT NULL,
    `name` VARCHAR(128) NOT NULL,
    `image` VARCHAR(128) NOT NULL,
    `quantity` INT NOT NULL,
    `description` VARCHAR(128) NOT NULL,
    `price` float NOT NULL,
    PRIMARY KEY (`id`)
);