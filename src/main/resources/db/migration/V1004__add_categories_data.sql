USE `onlineshop`;

INSERT INTO `categories` (`name`, `icon`)
VALUES ('Dishwasher', 'dishwasher.jpg'),
       ('Electric Kettle', 'electric-kettle.jpg'),
       ('Fridge', 'fridge.jpg'),
       ('Hair Dryer', 'hair-dryer.jpg'),
       ('Iron', 'iron.jpg'),
       ('Microwave Oven', 'microwave-oven.jpg'),
       ('Oven', 'oven.jpg'),
       ('Television', 'tv.jpg'),
       ('Vacuum Cleaner', 'vacuum-cleaner.jpg'),
       ('Washing Machine', 'washing-machine.png');