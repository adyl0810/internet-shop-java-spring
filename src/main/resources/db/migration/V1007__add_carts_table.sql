USE `onlineshop`;

CREATE TABLE `carts`
(
    `id` INT auto_increment NOT NULL,
    `user_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_carts_user`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
);