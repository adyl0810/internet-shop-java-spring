USE `onlineshop`;

CREATE TABLE `users`
(
    `id`       int auto_increment NOT NULL,
    `email`    varchar(128)       NOT NULL,
    `password` varchar(128)       NOT NULL,
    `fullname` varchar(128)       NOT NULL DEFAULT 'No Name',
    `active`   boolean            NOT NULL DEFAULT TRUE,
    `role`     varchar(24)        NOT NULL DEFAULT 'USER',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `email_unique` (`email` ASC)
);