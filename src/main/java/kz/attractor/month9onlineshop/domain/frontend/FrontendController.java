package kz.attractor.month9onlineshop.domain.frontend;

import kz.attractor.month9onlineshop.domain.brand.BrandDTO;
import kz.attractor.month9onlineshop.domain.brand.BrandService;
import kz.attractor.month9onlineshop.domain.cart.CartDTO;
import kz.attractor.month9onlineshop.domain.cart.CartService;
import kz.attractor.month9onlineshop.domain.cart_items.CartItemsDTO;
import kz.attractor.month9onlineshop.domain.cart_items.CartItemsService;
import kz.attractor.month9onlineshop.domain.category.CategoryDTO;
import kz.attractor.month9onlineshop.domain.category.CategoryService;
import kz.attractor.month9onlineshop.domain.product.ProductDTO;
import kz.attractor.month9onlineshop.domain.product.ProductService;
import kz.attractor.month9onlineshop.domain.user.UserDTO;
import kz.attractor.month9onlineshop.domain.user.UserRegisterForm;
import kz.attractor.month9onlineshop.domain.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontendController {

    private final CategoryService categoryService;
    private final ProductService productService;
    private final BrandService brandService;
    private final PropertiesService propertiesService;
    private final UserService userService;
    private final CartService cartService;
    private final CartItemsService cartItemsService;

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }

    @GetMapping
    public String index(Model model,
                        Pageable pageable,
                        HttpServletRequest uriBuilder,
                        HttpSession session,
                        Principal principal) {
        List<CategoryDTO> categories = categoryService.getCategories(pageable).getContent();
        List<BrandDTO> brands = brandService.getBrands(pageable).getContent();
        List<CartDTO> carts = cartService.getCarts();
        model.addAttribute("brands", brands);
        model.addAttribute("categories", categories);
        var products = productService.getProducts(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(products, propertiesService.getDefaultPageSize(), model, uri);
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        return "index";
    }

    @GetMapping("/product/{id}")
    public String getProduct(Model model,
                             Principal principal,
                             @PathVariable Integer id) {
        ProductDTO product = productService.getProduct(id);
        model.addAttribute("product", product);
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        return "product";
    }

    @PostMapping("/product/{id}")
    public String addProductToCart(@PathVariable Integer id,
                                   @RequestParam int quantity,
                                   Principal principal,
                                   HttpSession session) {
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            if (session.getAttribute("currentUserCart") == null) {
                cartService.createCartForUser(user);
                session.setAttribute("currentUserCart", cartService.getUserCart(user));
            }
            cartItemsService.addCartItem(cartService.getUserCart(user), productService.getProduct(id), quantity);
            return "redirect:/";
        }
        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String userProfile(Model model, Principal principal) {
        var user = userService.getByEmail(principal.getName());
        model.addAttribute("user", user);
        return "profile";
    }

    @GetMapping("/register")
    public String registerUser(Model model) {
        if (!model.containsAttribute("user"))
            model.addAttribute("user", new UserRegisterForm());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid UserRegisterForm form,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("user", form);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/register";
        }
        userService.register(form);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/cart")
    public String userCart(Model model,
                           HttpSession session,
                           Principal principal) {
        CartDTO cart = (CartDTO) session.getAttribute("currentUserCart");
        List<CartItemsDTO> cartItems = cartItemsService.getCartItems(cart);
        model.addAttribute("cart", cart);
        model.addAttribute("cartItems",cartItems);
        return "cart";
    }

    @PostMapping("/cart/{id}/delete")
    public String deleteProductFromCart(@PathVariable int id) {
        cartItemsService.deleteCartItemById(id);
        return "redirect:/cart";
    }

    @PostMapping("/cart/{id}/edit")
    public String editProductFromCart(@PathVariable int id,
                                      @RequestParam int quantityNew) {
        cartItemsService.editCartItemQuantityById(id, quantityNew);
        return "redirect:/cart";
    }

    @PostMapping("/cart/clear")
    public String clearCart(HttpSession session) {
        CartDTO cart = (CartDTO) session.getAttribute("currentUserCart");
        cartItemsService.clearCartItems(cart);
        return "redirect:/";
    }

    @PostMapping("/cart/order")
    public String orderCart(HttpSession session) {
        CartDTO cart = (CartDTO) session.getAttribute("currentUserCart");
        cartItemsService.changeCartItemsStatus(cart);
        return "redirect:/cart";
    }
}
