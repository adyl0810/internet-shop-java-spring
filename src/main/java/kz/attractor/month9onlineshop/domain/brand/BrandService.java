package kz.attractor.month9onlineshop.domain.brand;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BrandService {
    private final BrandRepository brandRepository;

    public Page<BrandDTO> getBrands(Pageable pageable) {
        return brandRepository.findAll(pageable)
                .map(BrandDTO::from);
    }

    public BrandDTO addBrand(BrandDTO brandData) {
        var brand = Brand.builder()
                .name(brandData.getName())
                .icon(brandData.getIcon())
                .build();
        brandRepository.save(brand);
        return BrandDTO.from(brand);
    }

    public void deleteBrand(int brandId) {
        brandRepository.deleteById(brandId);
    }
}
