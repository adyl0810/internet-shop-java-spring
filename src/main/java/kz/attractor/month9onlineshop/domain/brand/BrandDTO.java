package kz.attractor.month9onlineshop.domain.brand;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BrandDTO {

    @NotNull
    private int id;

    @NotBlank
    @Size(min = 2, max = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 128)
    private String icon;

    public static BrandDTO from(Brand brand) {
        return builder()
                .id(brand.getId())
                .name(brand.getName())
                .icon(brand.getIcon())
                .build();
    }
}
