package kz.attractor.month9onlineshop.domain.brand;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api/brands")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BrandController {
    private final BrandService brandService;

    @GetMapping
    public List<BrandDTO> getBrands(Pageable pageable) {
        return brandService.getBrands(pageable).getContent();
    }

    @PostMapping()
    public BrandDTO addBrand(@Valid @RequestBody BrandDTO brandData) {
        return brandService.addBrand(brandData);
    }


    @DeleteMapping("/{id}")
    public void deleteBrand(@PathVariable int id) {
        brandService.deleteBrand(id);
    }

    @ExceptionHandler(BindException.class)
    private ResponseEntity<?> handleBind(BindException ex) {
        var errors = ex.getFieldErrors()
                .stream()
                .map(fe -> String.format("%s -> %s",
                        fe.getField(), fe.getDefaultMessage()))
                .collect(toList());
        return ResponseEntity.unprocessableEntity()
                .body(errors);
    }
}
