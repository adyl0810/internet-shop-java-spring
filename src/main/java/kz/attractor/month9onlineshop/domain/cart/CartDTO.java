package kz.attractor.month9onlineshop.domain.cart;

import kz.attractor.month9onlineshop.domain.user.UserDTO;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class CartDTO {
    private int id;
    private UserDTO user;

    public static CartDTO from(Cart cart) {
        return builder()
                .id(cart.getId())
                .user(UserDTO.from(cart.getUser()))
                .build();
    }
}
