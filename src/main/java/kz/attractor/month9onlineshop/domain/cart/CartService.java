package kz.attractor.month9onlineshop.domain.cart;

import kz.attractor.month9onlineshop.domain.cart_items.CartItemsService;
import kz.attractor.month9onlineshop.domain.user.User;
import kz.attractor.month9onlineshop.domain.user.UserDTO;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CartService {

    CartRepository cartRepository;
    CartItemsService cartItemsService;

    public List<CartDTO> getCarts() {
        List<Cart> carts = cartRepository.findAll();
        return carts.stream()
                .map(CartDTO::from)
                .collect(Collectors.toList());
    }

    public CartDTO createCartForUser(UserDTO user) {
        Cart newCart = new Cart(new User(user.getId(), user.getEmail(), user.getFullname()));
        cartRepository.save(newCart);
        return CartDTO.from(newCart);
    }

    public boolean hasUserCart(UserDTO user, CartDTO cart) {
        return cart.getUser() == user;
    }

    public List<CartDTO> getUserCarts(UserDTO user) {
        List<Cart> carts = cartRepository.findAllByUserId(user.getId());
        return carts.stream()
                .map(CartDTO::from)
                .collect(Collectors.toList());
    }

    public CartDTO getUserCart(UserDTO user) {
        return getUserCarts(user).get(getUserCarts(user).size()-1);
    }

    public boolean isUserCartsEmpty(UserDTO user) {
        List<Cart> carts = cartRepository.findAllByUserId(user.getId());
        List<CartDTO> cartsDTO = carts.stream()
                .map(CartDTO::from)
                .collect(Collectors.toList());
        for (CartDTO cartDTO : cartsDTO) {
            if (cartItemsService.isCartEmpty(cartDTO))
                return true;
        }
        return false;
    }

}
