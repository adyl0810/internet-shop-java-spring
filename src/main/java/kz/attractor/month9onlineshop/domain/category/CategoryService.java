package kz.attractor.month9onlineshop.domain.category;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public Page<CategoryDTO> getCategories(Pageable pageable) {
        return categoryRepository.findAll(pageable)
                .map(CategoryDTO::from);
    }
}
