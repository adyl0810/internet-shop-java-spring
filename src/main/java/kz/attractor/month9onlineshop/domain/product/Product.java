package kz.attractor.month9onlineshop.domain.product;

import kz.attractor.month9onlineshop.domain.brand.Brand;
import kz.attractor.month9onlineshop.domain.category.Category;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    @NotNull
    @PositiveOrZero
    @Column
    private int quantity;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String description;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Brand brand;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @NotNull
    @PositiveOrZero
    @Column
    private float price;
}

