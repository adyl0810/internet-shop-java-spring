package kz.attractor.month9onlineshop.domain.product;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/products")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public List<ProductDTO> getProducts(@RequestParam(value = "name", required = false) String name,
                                        Pageable pageable) {
        if (name == null) {
            return productService.getProducts(pageable).getContent();
        } else {
            return productService.getProductsByName(pageable, name).getContent();
        }
    }
}
