package kz.attractor.month9onlineshop.domain.product;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductService {
    private final ProductRepository productRepository;

    public Page<ProductDTO> getProducts(Pageable pageable) {
        return productRepository.findAll(pageable)
                .map(ProductDTO::from);
    }

    public Page<ProductDTO> getProductsByName(Pageable pageable, String name) {
        return productRepository.findAllByName(name, pageable)
                .map(ProductDTO::from);
    }

    public ProductDTO getProduct(int id) {
        Product product = productRepository.getById(id);
        return ProductDTO.from(product);
    }
}
