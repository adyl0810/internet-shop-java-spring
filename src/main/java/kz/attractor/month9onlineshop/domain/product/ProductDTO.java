package kz.attractor.month9onlineshop.domain.product;

import kz.attractor.month9onlineshop.domain.brand.BrandDTO;
import kz.attractor.month9onlineshop.domain.category.CategoryDTO;
import lombok.*;

import javax.validation.constraints.*;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductDTO {

    @NotNull
    private int id;

    @NotBlank
    @Size(min = 2, max = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 128)
    private String image;

    @NotNull
    @PositiveOrZero
    private int quantity;

    @NotBlank
    @Size(min = 2, max = 128)
    private String description;

    @NotNull
    @PositiveOrZero
    private float price;

    @NotNull
    private BrandDTO brand;

    @NotNull
    private CategoryDTO category;

    public static ProductDTO from(Product product) {
        return builder()
                .id(product.getId())
                .name(product.getName())
                .image(product.getImage())
                .quantity(product.getQuantity())
                .description(product.getDescription())
                .price(product.getPrice())
                .brand(BrandDTO.from(product.getBrand()))
                .category(CategoryDTO.from(product.getCategory()))
                .build();
    }
}

