package kz.attractor.month9onlineshop.domain.cart_items;

import kz.attractor.month9onlineshop.domain.brand.BrandDTO;
import kz.attractor.month9onlineshop.domain.cart.CartDTO;
import kz.attractor.month9onlineshop.domain.category.CategoryDTO;
import kz.attractor.month9onlineshop.domain.product.Product;
import kz.attractor.month9onlineshop.domain.product.ProductDTO;
import lombok.*;

@Getter
@Setter
@ToString
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CartItemsDTO {

    private int id;
    private CartDTO cart;
    private ProductDTO product;
    private int quantity;
    private String status;

    static CartItemsDTO from(CartItems cartItems) {
        return builder()
                .id(cartItems.getId())
                .cart(CartDTO.from(cartItems.getCart()))
                .product(ProductDTO.from(cartItems.getProduct()))
                .quantity(cartItems.getQuantity())
                .status(cartItems.getStatus())
                .build();
    }
}
