package kz.attractor.month9onlineshop.domain.cart_items;

import kz.attractor.month9onlineshop.domain.cart.Cart;
import kz.attractor.month9onlineshop.domain.product.Product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cart_items")
@Entity
public class CartItems {
    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Cart cart;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Positive
    @Column
    private int quantity;

    @NotNull
    @Size(min = 2, max = 32)
    @Column
    private String status = "PENDING";

    public CartItems(Cart cart, Product product, int quantity, String status) {
        this.cart = cart;
        this.product = product;
        this.quantity = quantity;
        this.status = status;
    }
}
