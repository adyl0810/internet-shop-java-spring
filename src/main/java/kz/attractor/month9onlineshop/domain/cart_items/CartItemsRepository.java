package kz.attractor.month9onlineshop.domain.cart_items;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartItemsRepository extends JpaRepository<CartItems, Integer> {
    List<CartItems> findByCartId(int id);
}
