package kz.attractor.month9onlineshop.domain.cart_items;

import kz.attractor.month9onlineshop.domain.brand.Brand;
import kz.attractor.month9onlineshop.domain.cart.Cart;
import kz.attractor.month9onlineshop.domain.cart.CartDTO;
import kz.attractor.month9onlineshop.domain.category.Category;
import kz.attractor.month9onlineshop.domain.product.Product;
import kz.attractor.month9onlineshop.domain.product.ProductDTO;
import kz.attractor.month9onlineshop.domain.user.User;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CartItemsService {
    private final CartItemsRepository cartItemsRepository;

    public boolean isCartEmpty(CartDTO cart) {
        return cartItemsRepository.findByCartId(cart.getId()) == null;
    }

    public void addCartItem(CartDTO cart, ProductDTO product, int quantity) {
        CartItems cartItems = dtoToEntity(cart, product, quantity, "PENDING");
        cartItemsRepository.save(cartItems);
    }



    public List<CartItemsDTO> getCartItems(CartDTO cart) {
        return cartItemsRepository.findByCartId(cart.getId())
                .stream()
                .map(CartItemsDTO::from)
                .collect(Collectors.toList());
    }

    public CartItemsDTO getCartItem(int id) {
        CartItems cartItem = cartItemsRepository.getById(id);
        return CartItemsDTO.from(cartItem);
    }

    public void deleteCartItemById(int id) {
        cartItemsRepository.deleteById(id);
    }

    public void clearCartItems(CartDTO cart) {
        List<CartItemsDTO> cartItems = getCartItems(cart);
        for (CartItemsDTO cartItem : cartItems) {
            deleteCartItemById(cartItem.getId());
        }
    }

    public void editCartItemQuantityById(int id, int quantity) {
        CartItemsDTO cartItem = getCartItem(id);
        cartItem.setQuantity(quantity);
        CartItems cartItemEntity = dtoToEntity(cartItem.getCart(), cartItem.getProduct(), cartItem.getQuantity(), cartItem.getStatus());
        cartItemsRepository.save(cartItemEntity);
        deleteCartItemById(id);

    }

    public void changeCartItemsStatus(CartDTO cart) {
        List<CartItemsDTO> cartItems = getCartItems(cart);
        for (CartItemsDTO item : cartItems) {
            CartItemsDTO cartItem = getCartItem(item.getId());
            cartItem.setStatus("COMPLETED");
            CartItems cartItemEntity = dtoToEntity(cartItem.getCart(), cartItem.getProduct(), cartItem.getQuantity(), cartItem.getStatus());
            cartItemsRepository.save(cartItemEntity);
            deleteCartItemById(item.getId());
        }
    }

    public CartItems dtoToEntity(CartDTO cart, ProductDTO product, int quantity, String status) {
        User cartUser = new User(cart.getUser().getId(), cart.getUser().getEmail(), cart.getUser().getFullname());
        Cart cartEntity = new Cart(cart.getId(), cartUser);
        Brand brandEntity = new Brand(product.getBrand().getId(), product.getBrand().getName(), product.getBrand().getIcon());
        Category categoryEntity = new Category(product.getCategory().getId(), product.getCategory().getName(), product.getCategory().getIcon());
        Product productEntity = new Product(product.getId(), product.getName(), product.getImage(), product.getQuantity(), product.getDescription(), brandEntity, categoryEntity, product.getPrice());
        return new CartItems(cartEntity, productEntity, quantity, status);
    }
}
