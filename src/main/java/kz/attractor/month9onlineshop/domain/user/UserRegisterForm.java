package kz.attractor.month9onlineshop.domain.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserRegisterForm {
    @NotBlank
    @Email
    private String email = "";

    @Size(min = 3, max = 36, message = "Length must be >= 3 and <= 36")
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Name should contain only letters")
    private String name = "";

    @NotBlank
    private String password = "";
}
