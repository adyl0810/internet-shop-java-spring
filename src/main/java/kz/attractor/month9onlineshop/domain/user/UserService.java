package kz.attractor.month9onlineshop.domain.user;

import kz.attractor.month9onlineshop.exceptions.UserAlreadyExistsException;
import kz.attractor.month9onlineshop.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder _encoder;

    public UserDTO register(UserRegisterForm form) {
        if (userRepository.existsByEmail(form.getEmail()))
            throw new UserAlreadyExistsException();
        var user = User.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(_encoder.encode(form.getPassword()))
                .build();
        userRepository.save(user);
        return UserDTO.from(user);
    }

    public UserDTO getByEmail(String email) {
        var user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        return UserDTO.from(user);
    }

    public boolean isUserAuthenticated(String email) {
        return userRepository.existsByEmail(email);
    }
}
